package de.arkandos.gol;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Consumer;

public final class Cell implements Comparable<Cell> {
    public final int X;
    public final int Y;

    public Cell(int x, int y) {
        X = x;
        Y = y;
    }


    /**
     * Visit all neighbours and this cell, accumulating a state on the way
     */
    public <TState> TState foldNeighboursAndSelf(TState state, BiFunction<Cell, TState, TState> folder) {
        var stateCell = new Ref<>(state);

        iterNeighboursAndSelf(neighbour ->
            stateCell.set(folder.apply(neighbour, stateCell.get()))
        );

        return stateCell.get();
    }


    /**
     * Visit all neighbours of a cell, accumulating a state on the way
     */
    public <TState> TState foldNeighbours(TState initialState, BiFunction<Cell, TState, TState> folder) {
        return foldNeighboursAndSelf(initialState, (cell, state) ->
            (cell.equals(this)) ? state : folder.apply(cell, state)
        );
    }


    /**
     * Visit all neighbours, and this cell
     */
    public void iterNeighboursAndSelf(Consumer<Cell> visitor) {
        for(int  x = X - 1; x <= X + 1; ++x) {
            for(int y = Y - 1; y <= Y + 1; ++y) {
                visitor.accept(new Cell(x, y));
            }
        }
    }


    /**
     * Visit only neighbours.
     */
    public void iterNeighbours(Consumer<Cell> visitor) {
        iterNeighboursAndSelf(cell -> {
            if (!cell.equals(this)) visitor.accept(cell);
        });
    }


    // region Comparable<Cell> implementation
    /**
     * Compares 2 Cells.
     * Cells are ordered top-down, left to right on a coordinate system with a top-left origin.
     */
    public int compareTo(Cell other) {
        var y = Integer.compare(Y, other.Y);
        if (y != 0) return y;

        var x = Integer.compare(X, other.X);
        if (x != 0) return x;

        return 0;
    }
    // endregion


    // "IEquatable<Cell>" implementation
    public boolean equals(Cell other) {
        return (X == other.X && Y == other.Y);
    }


    @Override
    public boolean equals(Object objOther) {
        if (objOther instanceof Cell) {
            return equals((Cell) objOther);
        } else {
            return false;
        }
    }


    @Override
    public int hashCode() {
        return Objects.hash(X, Y);
    }
    // endregion


    public String toString() {
        return String.format("(%d, %d)", X, Y);
    }
}
