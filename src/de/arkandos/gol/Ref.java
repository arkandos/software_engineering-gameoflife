package de.arkandos.gol;

/**
 * Interior mutability container
 * to mutate values inside of lambdas :)
 */
public final class Ref<T> {
    private T value;


    public Ref(T value) {
        this.value = value;
    }


    public T get() {
        return value;
    }


    public Ref<T> set(T value) {
        this.value = value;
        return this;
    }
}
