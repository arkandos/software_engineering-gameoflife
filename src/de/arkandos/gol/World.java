package de.arkandos.gol;

import java.util.*;
import java.util.function.Supplier;

public final class World implements Iterable<Cell> {
    /*public*/ final TreeSet<Cell> Cells; // cannot expose Set<Cell> because there are no immutable interfaces in java.
    public final int Generation;


    private World(TreeSet<Cell> cells, int generation) {
        Cells = cells;
        Generation = generation;
    }

    public World(Iterable<Cell> initialCells) {
        // "this()" has to be the first line of a ctor
        // - let's borrow a pattern from javascript then!
        //   the cast is kinda ugly though.

        // also, why isn't there a TreeSet<> ctor for this?
        // the only one I could find takes a Collection<> which seems to be too much
        // (why would the TreeSet<> need the ability to add items to the source sequence it is constructed from?)

        this(((Supplier<TreeSet<Cell>>)() -> {
            var result = new TreeSet<Cell>();
            for(var item : initialCells)  result.add(item);
            return result;
        }).get(), 0);
    }

    // Cell[] does not implement Iterable<Cell> and there is (of course) no TreeSet<> ctor - JAVA PLS
    public World(Cell[] initialCells) {
        // s.o.
        this(((Supplier<TreeSet<Cell>>)() -> {
            var result = new TreeSet<Cell>();
            for(int i = 0; i < initialCells.length; ++i)  result.add(initialCells[i]);
            return result;
        }).get(), 0);
    }


    /**
     * State (Dead or Alive) of a cell.
     */
    public CellState cellState(Cell cell) {
        return (Cells.contains(cell) ? CellState.Alive : CellState.Dead);
    }

    /**
     * Number of alive neighbours of a cell.
     */
    public int neighbours(Cell cell) {
        return cell.foldNeighbours(0, (neighbour, count) ->
                (cellState(neighbour) == CellState.Alive ? count + 1 : count)
        );
    }

    /**
     * Number of alive cells.
     */
    public int population() {
        return Cells.size();
    }

    /**
     * The next generation of this world based on some rules.
     * @return A brave, new World
     */
    public World next(RulesOfLife rules) {
        var newCells = new TreeSet<Cell>();

        for(Cell aliveCell : Cells) {
            aliveCell.iterNeighboursAndSelf(cell -> {
                if (rules.next(cellState(cell), neighbours(cell)) == CellState.Alive) {
                    newCells.add(cell);
                }
            });
        }

        return new World(newCells, Generation + 1);
    }


    // region Iterable<Cell> implementation
    /**
     * Iterate over all alive cells in this world.
     */
    public Iterator<Cell> iterator() {
        return Cells.iterator();
    }

    // not really, but nice to have
    public int size() {
        return population();
    }
    // endregion
}
