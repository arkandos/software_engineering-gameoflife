package de.arkandos.gol.tests;

import de.arkandos.gol.CellState;
import de.arkandos.gol.RulesOfLife;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class RulesOfLifeTest {
    // This only tests the default implementation of the rules.


    @Test
    void aliveCellDiesOfStarvation() {
        var rules = new RulesOfLife();
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 0));
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 1));
    }

    @Test
    void aliveCellDiesOfOverPopulation() {
        var rules = new RulesOfLife();
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 4));
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 5));
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 6));
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 7));
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 8));
        assertEquals(CellState.Dead, rules.next(CellState.Alive, 9));
    }

    @Test
    void aliveCellSurvives() {
        var rules = new RulesOfLife();
        assertEquals(CellState.Alive, rules.next(CellState.Alive, 2));
        assertEquals(CellState.Alive, rules.next(CellState.Alive, 3));
    }

    @Test
    void birth() {
        var rules = new RulesOfLife();
        assertEquals(CellState.Alive, rules.next(CellState.Dead, 3));
    }

    @Test
    void notBirth() {
        var rules = new RulesOfLife();
        for(int neighbours = 0; neighbours < 10; ++neighbours) {
            if (neighbours != 3) {
                assertEquals(CellState.Dead, rules.next(CellState.Dead, neighbours));
            }
        }
    }

    @Test
    void toStringIsIdentity() {
        // Ideally we could generate a random string here and test based on the property.

        var rules = new RulesOfLife();
        assertEquals(rules.toString(), new RulesOfLife(rules.toString()).toString());
    }
}
