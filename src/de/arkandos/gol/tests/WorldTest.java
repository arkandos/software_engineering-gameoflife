package de.arkandos.gol.tests;

import de.arkandos.gol.Cell;
import de.arkandos.gol.CellState;
import de.arkandos.gol.RulesOfLife;
import de.arkandos.gol.World;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class WorldTest {

    @Test
    public void constructedCellsAreAlive() {
        var cells = new Cell[] {
                new Cell(0, 1),
                new Cell(1, 2),
                new Cell(2, 2),
                new Cell(2, 1),
                new Cell(2, 0)
        };

        var world = new World(cells);

        assertEquals(cells.length, world.population());
        for(int i = 0; i < cells.length; ++i) {
            assertEquals(CellState.Alive, world.cellState(cells[i]));
        }
    }

    @Test
    public void iteratingOverTheWorldGivesUsOurAliveCellsBack() {
        var cells = Arrays.asList(
                new Cell(0, 1),
                new Cell(1, 2),
                new Cell(2, 2),
                new Cell(2, 1),
                new Cell(2, 0)
        );


        var world = new World(cells);

        assertEquals(cells.size(), world.population());
        for(Cell cell : world) {
            assertEquals(CellState.Alive, world.cellState(cell));
            assertTrue(cells.indexOf(cell) >= 0);
        }
    }

    @Test
    public void otherCellsAroundTheAreaAreDead() {
        int min = -100, max = 100;

        var cells = new Cell[] {
                new Cell(0, 1),
                new Cell(1, 2),
                new Cell(2, 2),
                new Cell(2, 1),
                new Cell(2, 0)
        };

        var world = new World(cells);

        var aliveCells = 0;
        for(int x = min; x <= max; ++x) {
            for(int y = min; y <= max; ++y) {
                var cell = new Cell(x, y);
                if (world.cellState(cell) == CellState.Alive) {
                    aliveCells += 1;
                }
            }
        }

        assertEquals(cells.length, aliveCells);
        assertEquals(cells.length, world.population());
    }

    @Test
    public void everyCellInABlockHasExactlyThreeNeighbours() {
        // Neighbours is kinda annoying to test...
        // let's just try out some case.

        var cells = new Cell[] {
                new Cell(1, 1),
                new Cell(1, 2),
                new Cell(2, 2),
                new Cell(2, 1)
        };

        var world = new World(cells);

        for(int i = 0; i < cells.length; ++i) {
            assertEquals(3, world.neighbours(cells[i]));
        }
    }

    // region Common Patterns
    @Test
    public void barge() {
        // A still life that is more interesting than a simple block.

        var rule = new RulesOfLife();

        var cells = new Cell[] {
                new Cell(2, 3),
                new Cell(3, 2),
                new Cell(3, 4),
                new Cell(4, 3),
                new Cell(4, 5),
                new Cell(5, 4)
        };

        var world = new World(cells);

        var world2 = world.next(rule);

        assertEquals(cells.length, world2.population());

        for(int i = 0; i < cells.length; ++i) {
            assertEquals(CellState.Alive, world2.cellState(cells[i]));
        }
    }

    @Test
    public void blinker() {
        var rule = new RulesOfLife();

        var cells0 = new Cell[] {
                new Cell(-1, 0),
                new Cell(0, 0),
                new Cell(1, 0)
        };
        var cells1 = new Cell[] {
                new Cell(0, -1),
                new Cell(0, 0),
                new Cell(0, 1)
        };

        var world0 = new World(cells0);

        var world1 = world0.next(rule);
        var world2 = world1.next(rule);

        assertEquals(3, world0.population());
        assertEquals(3, world1.population());
        assertEquals(3, world2.population());

        for(int i = 0; i < 3; ++i) {
            assertEquals(CellState.Alive, world1.cellState(cells1[i]));
        }

        for(int i = 0; i < 3; ++i) {
            assertEquals(CellState.Alive, world2.cellState(cells0[i]));
        }
    }

    @Test
    public void glider() {
        var rule = new RulesOfLife();

        var world = new World(new Cell[] {
                new Cell(0, 1),
                new Cell(1, 2),
                new Cell(2, 2),
                new Cell(2, 1),
                new Cell(2, 0)
        });

        var world2 = world.next(rule).next(rule).next(rule).next(rule);

        assertEquals(world.population(), world2.population());

        for(Cell cell : world) {
            var cell2 = new Cell(cell.X + 1, cell.Y + 1);
            assertEquals(CellState.Alive, world2.cellState(cell2));
        }
    }
    // endregion
}