package de.arkandos.gol;

public final class Main {

    public static void main(String[] args) {
        var rule = new RulesOfLife();
        var world = new World(new Cell[] {
                new Cell(0, 1),
                new Cell(1, 2),
                new Cell(2, 2),
                new Cell(2, 1),
                new Cell(2, 0)
        });

        world = world.next(rule).next(rule).next(rule).next(rule);

        System.out.println("Hello, Sailor!");

    }
}
