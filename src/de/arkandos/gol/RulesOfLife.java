package de.arkandos.gol;

import java.util.TreeSet;
import java.util.stream.Collectors;

public final class RulesOfLife {
    private final TreeSet<Integer> survive;
    private final TreeSet<Integer> birth;


    /**
     * GoL rules written in the classic S/B notation.
     * @throws IllegalArgumentException on parse error.
     */
    public RulesOfLife(String sb) {
        var splitIdx = sb.indexOf('/');

        survive = parseRuleStringPart(sb, 0, splitIdx);
        birth = parseRuleStringPart(sb, splitIdx + 1, sb.length());
    }

    private TreeSet<Integer> parseRuleStringPart(String sb, int startIdx, int endIdx) {
        var result = new TreeSet<Integer>();

        for(int i = startIdx; i < endIdx; ++i) {
            var ch = sb.charAt(i);
            var digit = Character.digit(ch, 10);

            if (digit < 0 || digit >= 10) {
                throw new IllegalArgumentException(
                        String.format("Unexpected character at position %d: '%c', expected a digit (0-9) instead!",
                                i, ch));
            }

            result.add(digit);
        }

        return result;
    }

    /**
     * Classic GoL rules.
     */
    public RulesOfLife() {
        this("23/3");
    }


    /**
     * Based on the rules of the game, return whether or not a cell should be alive in the next generation.
     * @param state Current state of a cell
     * @param neighbours Number of neighbours of that cell
     * @return State of that cell in the next generation
     */
    public CellState next(CellState state, int neighbours) {
        var setToCheck = (state == CellState.Alive ? survive : birth);
        return (setToCheck.contains(neighbours) ? CellState.Alive : CellState.Dead);
    }


    /**
     * S/B representation of this rule, such that
     * ruleOfLife.toString() == (new RuleOfLife(ruleOfLife.toString()).toString()
     */
    @Override
    public String toString() {
        var s = survive.stream().map(Object::toString).collect(Collectors.joining(""));
        var b = birth.stream().map(Object::toString).collect(Collectors.joining(""));

        return String.format("%s/%s", s, b);
    }


    // region IEquatable<RuleOfLife> implementation
    public boolean equals(RulesOfLife other) {
        return this.toString().equals(other.toString());
    }


    @Override
    public boolean equals(Object otherObj) {
        if (otherObj instanceof RulesOfLife) {
            return equals((RulesOfLife) otherObj);
        } else {
            return false;
        }
    }


    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }
    //endregion
}
